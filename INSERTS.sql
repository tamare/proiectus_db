
/*
 * INSERTS PARA REALIZAR PRUEBAS
 * DESCRIPCIÓN: SPRINT 3
 * AUTORES: DAW 2
 *
 * CAMBIOS:
 * #01#  
 * #02# 
 * #03# 
 */
 
/* tabla city */
insert into CITY (cityname, regionstate, postalcode) VALUES
('Alcanar', 'Montsià', 43530),
('Amposta', 'Montsià', 43870),
('La Sénia', 'Montsià', 4356),
('Freginals', 'Montsià', 43558),
('La Galera', 'Montsià', 43515),
('Godall', 'Montsià', 43516),
('Mas de Barberans', 'Montsià', 43514),
('Masdenverge', 'Montsià', 43878),
('Sant Carles de la Ràpita', 'Montsià', 43540),
('Sant Jaume d\'enveja', 'Montsià', 43877),
('Santa Bàrbara', 'Montsià', 43570),
('Ulldecona', 'Montsià', 43550);

/* tabla role */
insert into ROLE (rolename) VALUES
('Alumne'),
('Professor'),
('Superadmin'),
('Admin');


/* tabla user */
create table USER (
    id_user int primary key auto_increment,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    username varchar(50) not null,
    passwd varchar(50) not null,
    profilepic text, -- tipo de dato text: no puede pertenecer a un index /// longitud máxima: 65,535 caracteres
    email varchar(100) not null,
    id_city int not null,
    bio text,
    id_role int not null,
    dni varchar(10) not null,
    birthdate date not null,
    creationdate datetime not null,
	status enum('active', 'inactive') default 'active',
    unique (dni),
    unique (email),
    unique (username),
    foreign key (id_city) references CITY (id_city)
	on delete cascade
	on update cascade,
    foreign key (id_role) references ROLE (id_role)
	on delete cascade
	on update cascade
);

insert into USER () VALUES


-- para borrar constraints:
/* alter table user
drop unique (dni)
drop primary key; */

-- para añadir constraints:
/* alter table user
add primary key (iduser); */

-- posibles índices
/* alter table user
drop index idx_username; */
create index idx_username
    on USER (username);
    
create index idx_email
    on USER (email);
    

/* tabla school */
create table SCHOOL (
    id_school int primary key auto_increment,
    schoolname varchar(100) not null,
    schoolemail varchar(100) not null,
    address varchar(100) not null,
    id_city int not null,
    phone int not null,
    schooltype varchar(50) not null,
    foreign key (id_city) references CITY (id_city)
	on delete cascade
	on update cascade,
	unique (schoolemail)
);

/* tabla school_for_validation */
create table SCHOOL_FOR_VALIDATION (
    id_school int primary key auto_increment,
    schoolname varchar(100) not null,
    schoolemail varchar(100) not null,
    address varchar(100) not null,
    id_city int not null,
    phone int not null,
    schooltype varchar(50) not null,
    foreign key (id_city) references CITY (id_city)
	on delete cascade
	on update cascade,
	unique (schoolemail)
);


/* tabla school_users */
create table SCHOOL_USERS (
    id_schooluser int primary key auto_increment,
    id_user int not null,
    id_school int not null,
    foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade,
    foreign key (id_school) references SCHOOL (id_school)
	on delete cascade
	on update cascade
);


/* tabla proposal */
create table PROPOSAL (
    id_proposal int primary key auto_increment,
    proposal_name varchar(100) not null,
    publication_date date not null,
    specs varchar(100) not null,
    description varchar(100) not null,
    professional_family varchar(50) not null,
    limit_date date not null,
	status enum('active','inactive','deleted') not null
);


-- posibles índices
create index idx_proposal_name
    on PROPOSAL (proposal_name);

create index idx_pub_date
    on PROPOSAL (publication_date);

create index idx_limit_date
    on PROPOSAL (limit_date);

create index idx_family
    on PROPOSAL (professional_family);


/* tabla user_proposals */
create table USER_PROPOSALS (
    id_userprop int primary key auto_increment,
    id_user int not null,
    id_proposal int not null,
    publicationdate datetime not null,
    foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade,
    foreign key (id_proposal) references PROPOSAL (id_proposal)
	on delete cascade
	on update cascade
);


/* tabla school_proposals */
create table SCHOOL_PROPOSALS (
    id_schoolprop int primary key auto_increment,
    id_school int not null,
    id_proposal int not null,
    foreign key (id_school) references SCHOOL (id_school)
	on delete cascade
	on update cascade,
    foreign key (id_proposal) references PROPOSAL (id_proposal)
	on delete cascade
	on update cascade
);


/*tabla company*/
create table COMPANY (
	id_company int primary key auto_increment,
	email varchar(100) not null,
	name varchar(50) not null,
	nif varchar(10) not null,
	address varchar(100) not null,
	city varchar(50) not null,
	postal_code int not null,
	phone_number varchar(50) not null,
	sector varchar(50) not null,
	id_city int not null,
	status enum('active','inactive') not null,
	unique(id_company),
	unique(nif),
	unique(email),
	foreign key (id_city) references CITY (id_city)
	on delete cascade
	on update cascade
);

/*tabla company for validation*/
create table COMPANY_FOR_VALIDATION (
	id_company int primary key auto_increment,
	email varchar(100) not null,
	name varchar(50) not null,
	nif varchar(10) not null,
	address varchar(100) not null,
	city varchar(50) not null,
	postal_code int not null,
	phone_number varchar(50) not null,
	sector varchar(50) not null,
	id_city int not null,
	unique(id_company),
	unique(nif),
	unique(email),
	foreign key (id_city) references CITY (id_city)
	on delete cascade
	on update cascade
);


/* tabla company_user */
create table COMPANY_USERS (
	id_company_user int primary key auto_increment,
	id_user int not null,
	id_company int not null,
	foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade,
	foreign key (id_company) references COMPANY (id_company)
	on delete cascade
	on update cascade
);


/* taula tag */
create table TAG (
	id_tag int primary key auto_increment,
	tagname varchar(50) not null
);


/* taula proposal_tags */
create table PROPOSAL_TAGS (
	id_proposaltag int primary key auto_increment,
	id_proposal int not null,
	id_tag int not null,
	foreign key (id_proposal) references PROPOSAL (id_proposal)
	on delete cascade
	on update cascade,
	foreign key (id_tag) references TAG (id_tag)
	on delete cascade
	on update cascade
);


/* taula project*/
create table PROJECT (
    id_project int primary key auto_increment,
    project_name varchar(50) not null,
    initial_date date not null,
    ending_date date not null,
    budget int not null,
	description varchar(100) not null,
    professional_family varchar(50) not null,
	status enum('active', 'inactive') not null,
    foreign key (id_project) references PROPOSAL (id_proposal)
	on delete cascade
	on update cascade
);

/* tabla user projects */
create table USER_PROJECTS (
	id_userprojects int PRIMARY KEY AUTO_INCREMENT,
	id_user int NOT NULL,
	id_project int NOT NULL,
	foreign key (id_user) references USER (id_user)
	on delete CASCADE
	on update CASCADE,
	foreign key (id_project) references PROJECT (id_project)
	on delete CASCADE
	on update CASCADE
);

/*taula document_manager*/
create table DOCUMENT_MANAGER (
	id_manager int primary key auto_increment,
	size int not null,
	dm_owner int not null,
	id_project int not null,
	foreign key (dm_owner) references USER (id_user)
	on delete cascade
	on update cascade,
	foreign key (id_project) references PROJECT (id_project)
	on delete cascade
	on update cascade
);


/* taula dm_folder */
create table DM_FOLDER (
    id_folder int primary key auto_increment,
    folder_name varchar (50) not null,
    creation_date date not null,
    id_document_manager int not null,
    foreign key (id_document_manager) references DOCUMENT_MANAGER (id_manager)
	on delete cascade
	on update cascade
);


/* taula dm_file */
create table DM_FILE (
	id_file int primary key auto_increment,
	name varchar(100) not null,
	last_modification date not null,
	size int not null,
	file_type varchar(20) not null,
	id_folder int not null,
	foreign key (id_folder) references DM_FOLDER (id_folder)
	on delete cascade
	on update cascade
); 


/*taula dm_container_folder*/
create table DM_CONTAINER_FOLDER (
    id_container_folder int primary key auto_increment,
    id_folder int not null,
    id_parent_folder int
);


-- blog
create table BLOG (
    id_project int primary key auto_increment,
    title varchar(100) not null,
	foreign key (id_project) references PROJECT (id_project)
	on delete cascade
	on update cascade
);


-- post
create table POST (
    id_post int primary key auto_increment,
    id_project int not null,
	title varchar(50) not null,
	content longtext not null,
	creation_date date not null,
	last_modified date,
	id_user int not null,
	status enum('active','inactive') not null,
	foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade,
	foreign key (id_project) references BLOG (id_project)
	on delete cascade
	on update cascade
);

-- comment
create table COMMENT (
    id_comment  int primary key auto_increment,
    content  longtext not null,
    id_user  int not null,
    id_post  int not null,
    id_project int not null,
	foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade,
	foreign key (id_post) references POST (id_post)
	on delete cascade
	on update cascade,
	foreign key (id_project) references POST (id_project)
	on delete cascade
	on update cascade
);


-- vote
create table VOTE (
    id_post int not null,
    id_project int not null,
	id_user int not null,
	value_vote enum('1', '-1') not null,
	primary key (id_post, id_project, id_user)
);

-- wiki
create table WIKI (
    id_project int primary key auto_increment,
    title varchar(100) not null,
    foreign key (id_project) references PROJECT (id_project)
	on delete cascade
	on update cascade
);

-- article
create table ARTICLE (
    id_article int primary key auto_increment,
    id_project int not null,
    title varchar(50) not null,
    content longtext not null,
    creation_date date not null,
    last_modified date,
	reference longtext,
	id_user int,
	status enum('active','inactive') not null,
	foreign key (id_project) references WIKI (id_project)
	on delete cascade
	on update cascade,
	foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade
);

-- chat
create table CHAT (
    id_chat int primary key auto_increment,
    owner int not null,
	chat_name varchar (30) not null,
	description text
);

-- chat_member
create table CHAT_MEMBER (
    id_chat int not null,
    id_user int not null,
	foreign key (id_chat) references CHAT (id_chat)
	on delete cascade
	on update cascade,
	foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade
);

-- añadir fk a tabla chat
alter table CHAT
add constraint 
foreign key (owner) references CHAT_MEMBER (id_user) on delete cascade on update cascade;


-- message
create table MESSAGE (
    id_chat int not null,
    id_user int not null,
    content longtext not null,
    datetime datetime not null,
    primary key (id_user, datetime),
    foreign key (id_chat) references CHAT (id_chat)
	on delete cascade
	on update cascade,
    foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade
);

-- mail_message
create table MAIL_MESSAGE (
    id_message int primary key auto_increment,
    id_user int not null,
    reciever int not null,
    subject varchar(200) not null,
    content longtext not null,
    datetime datetime not null,
	delete_sender enum('no','yes') not null,
	delete_receiver enum('no','yes') not null,
    foreign key (id_user) references USER (id_user)
	on delete cascade
	on update cascade,
    foreign key (reciever) references USER (id_user)
	on delete cascade
	on update cascade
);

-----------------

-- resource center
create table RESOURCE_CENTER (
	id_rc int primary key auto_increment,
	size int not null,
	rc_manager int not null,
	foreign key (rc_manager) references USER (id_user)
	on delete cascade
	on update cascade
);

-- rc folder
create table RC_FOLDER (
	id_folder int primary key auto_increment,
	name varchar(50) not null,
	creation_date datetime not null,
	id_rc int not null,
	foreign key (id_rc) references RESOURCE_CENTER (id_rc)
	on delete cascade
	on update cascade
);

-- rc container folder
create table RC_CONTAINER_FOLDER (
	id_container_folder int primary key auto_increment,
	id_folder int not null,
	id_parent_folder int not null,
	foreign key (id_folder) references RC_FOLDER (id_folder)
	on delete cascade
	on update cascade
);

-- rc file
create table RC_FILE (
	id_file int primary key auto_increment,
	id_folder int not null,
	name varchar(50) not null,
	last_modification datetime not null,
	size int not null,
	file_type varchar(20) not null,
	foreign key (id_folder) references RC_FOLDER (id_folder)
	on delete cascade
	on update cascade
);

-- ticket
create table TICKET (
  id_incidence int primary key auto_increment,
  topic varchar(50) not null,
  type varchar(50) not null,
  priority varchar(50) not null,
  creation_date datetime not null,
  solved_date datetime not null,
  id_assigned_user int not null,
  id_author int not null,
  status enum('pending','in progress','resolved') not null,
  foreign key (id_assigned_user) references USER (id_user)
  on delete cascade
  on update cascade,
  foreign key (id_author) references USER (id_user)
  on delete cascade
  on update cascade
);




